<?php

/**
 * Load the assets.
 */
function swivelgroup_assests()
{
  wp_enqueue_style('swivelgroup-style', get_template_directory_uri() . '/dist/css/style.css', array(), VERSION, 'all');
  wp_enqueue_script('swivelgroup-main-js', get_template_directory_uri() . '/dist/js/main.js', array(), VERSION, true);
}
add_action('wp_enqueue_scripts', 'swivelgroup_assests');

/**
 * Load the editor assets.
 */
function swivelgroup_editor_styles()
{
  add_editor_style('/dist/css/style.css');
}
add_action('admin_init', 'swivelgroup_editor_styles');

/**
 * Add custom file type for the media upload.
 */
function swivelgroup_mime_types($mimes)
{
  // New allowed mime types.
  $mimes['svg'] = 'image/svg+xml';
  $mimes['svgz'] = 'image/svg+xml';
  $mimes['doc'] = 'application/msword';

  // Optional. Remove a mime type.
  unset($mimes['exe']);

  return $mimes;
}
add_filter('upload_mimes', 'swivelgroup_mime_types');


/* 
 * Remove WordPress Version Information with Code
 */
remove_action('wp_head', 'wp_generator');


/*
 * To remove empty paragraph tags from the page content
 */
function remove_empty_p($content)
{
  $content = force_balance_tags($content);
  $content = preg_replace('#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content);
  $content = preg_replace('~\s?<p>(\s|&nbsp;)+</p>\s?~', '', $content);
  return $content;
}


/*
 * To remove WP embed script
 */
function speed_stop_loading_wp_embed()
{
  if (!is_admin()) {
    wp_deregister_script('wp-embed');
  }
}
add_action('init', 'speed_stop_loading_wp_embed');


/*
 * To remove WP emoji
 */
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

/*
 * To remove WP admin emoji
 */
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('admin_print_styles', 'print_emoji_styles');


// Remove comments from post and pages
add_action('init', 'remove_comment_support', 100);
function remove_comment_support()
{
  remove_post_type_support('post', 'comments');
  remove_post_type_support('page', 'comments');
}