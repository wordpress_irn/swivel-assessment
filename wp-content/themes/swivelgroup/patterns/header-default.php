<?php
/**
 * Title: Default Header
 * Slug: swivelgroup/header-default
 * Categories: header
 * Block Types: core/template-part/header
 */
?>
<!-- wp:group {"backgroundColor":"contrast","textColor":"base","className":"container","layout":{"type":"constrained"}} -->
<div class="wp-block-group container has-base-color has-contrast-background-color has-text-color has-background">
	<!-- wp:group {"align":"wide","layout":{"type":"flex","justifyContent":"space-between"}} -->
	<div class="wp-block-group alignwide"><!-- wp:site-logo {"shouldSyncIcon":true} /-->

		<!-- wp:navigation {"ref":14,"textColor":"base","overlayBackgroundColor":"base","overlayTextColor":"contrast","layout":{"type":"flex","setCascadingProperties":true,"justifyContent":"right"}} /-->
	</div>
	<!-- /wp:group -->
</div>
<!-- /wp:group -->