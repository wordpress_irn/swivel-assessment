<?php
/**
 * Title: Default Footer
 * Slug: swivelgroup/footer-default
 * Categories: footer
 * Block Types: core/template-part/footer
 */
?>
<!-- wp:group {"backgroundColor":"contrast","className":"container","layout":{"type":"constrained"}} -->
<div class="wp-block-group container has-contrast-background-color has-background">
	<!-- wp:group {"align":"wide","style":{"spacing":{"padding":{"top":"var:preset|spacing|40"}}},"layout":{"type":"flex","justifyContent":"space-between"}} -->
	<div class="wp-block-group alignwide" style="padding-top:var(--wp--preset--spacing--40)"><!-- wp:columns -->
		<div class="wp-block-columns"><!-- wp:column {"width":""} -->
			<div class="wp-block-column"><!-- wp:site-logo /-->

				<!-- wp:heading {"level":5,"textColor":"base"} -->
				<h5 class="wp-block-heading has-base-color has-text-color">Contact</h5>
				<!-- /wp:heading -->

				<!-- wp:paragraph {"textColor":"white"} -->
				<p class="has-white-color has-text-color">It is a long established fact that a reader will be distracted by the
					readable content of a page when looking
					at its layout</p>
				<!-- /wp:paragraph -->
			</div>
			<!-- /wp:column -->

			<!-- wp:column -->
			<div class="wp-block-column"><!-- wp:heading {"level":5,"textColor":"base"} -->
				<h5 class="wp-block-heading has-base-color has-text-color">Menu</h5>
				<!-- /wp:heading -->

				<!-- wp:navigation {"ref":20,"textColor":"base","overlayMenu":"never","layout":{"type":"flex","justifyContent":"left","orientation":"vertical"}} /-->
			</div>
			<!-- /wp:column -->

			<!-- wp:column -->
			<div class="wp-block-column"><!-- wp:heading {"level":5,"textColor":"tertiary"} -->
				<h5 class="wp-block-heading has-tertiary-color has-text-color">OUR APPS</h5>
				<!-- /wp:heading -->

				<!-- wp:paragraph {"textColor":"white"} -->
				<p class="has-white-color has-text-color">Discover Eventchamp's innovative apps! Enhance attendee experience,
					manage events on-the-go, and stay
					connected through our suite of mobile solutions.</p>
				<!-- /wp:paragraph -->
			</div>
			<!-- /wp:column -->
		</div>
		<!-- /wp:columns -->
	</div>
	<!-- /wp:group -->

	<!-- wp:group {"align":"wide","style":{"spacing":{"padding":{"top":"var:preset|spacing|40"}}},"layout":{"type":"flex","justifyContent":"right"}} -->
	<div class="wp-block-group alignwide" style="padding-top:var(--wp--preset--spacing--40)">
		<!-- wp:paragraph {"textColor":"white"} -->
		<p class="has-white-color has-text-color"> Proudly powered by <a
				href="https://gitlab.com/wordpress_irn/swivel-assessment">irnsolutions</a> </p>
		<!-- /wp:paragraph -->
	</div>
	<!-- /wp:group -->
</div>
<!-- /wp:group -->