const path = require("path"),
  {globSync} = require("glob"),
  MiniCssExtractPlugin = require("mini-css-extract-plugin"),
  RemoveEmptyScriptsPlugin = require("webpack-remove-empty-scripts"),
  webpack = require("webpack"),
  autoprefixer = require("autoprefixer");

module.exports = {
  mode: "production",
  entry: {
    "css/style": "./assets/scss/style.scss",
    "js/main": "./assets/js/main.js",
  },
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "[name].js",
    clean: true,
  },
  resolve: {
    alias: {
      "@node_modules": path.resolve(__dirname, "node_modules"),
    },
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            // Interprets `@import` and `url()` like `import/require()` and will resolve them
            loader: "css-loader",
          },
          {
            // Loader for webpack to process CSS with PostCSS
            loader: "postcss-loader",
            options: {
              postcssOptions: {
                plugins: () => [autoprefixer],
              },
            },
          },
          {
            // Loads a SASS/SCSS file and compiles it to CSS
            loader: "sass-loader",
          },
        ],
      },
    ],
  },
  plugins: [
    new RemoveEmptyScriptsPlugin(),
    new MiniCssExtractPlugin(),
    new webpack.SourceMapDevToolPlugin({filename: "[name][ext].map"}),
  ],
};
