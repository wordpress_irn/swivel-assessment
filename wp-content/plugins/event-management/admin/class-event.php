<?php

namespace Admin;

use Admin\CustomPostType;

/**
 * Define the event post type functionality
 *
 * Loads and defines the event post type files of this plugin.
 *
 * @link       https://gitlab.com/wordpress_irn/swivel-assessment
 * @since      1.0.0
 *
 * @package    Event_Management
 * @subpackage Event_Management/admin
 */
if (!class_exists('Event')) {
  class Event extends CustomPostType
  {
    protected $POST_PER_PAGE = 3;

    /**
     * Initialize the event post type functionalities of the plugin.
     *
     * @since    1.0.0
     * @param string $post_type         post type name (singular)
     * @param string $post_type_plural  post type name (plural)
     */
    public function __construct($post_type, $post_type_plural)
    {
      parent::__construct($post_type, $post_type_plural);
      add_action('add_meta_boxes', [$this, 'addPostMetaBoxes']);
      add_action('save_post', [$this, 'saveMetaBox']);
      add_filter("manage_{$this->post_type}_posts_columns", [$this, 'eventTableHead']);
      add_action("manage_{$this->post_type}_posts_custom_column", [$this, 'eventTableContent'], 10, 2);
      add_shortcode('event_list', [$this, 'eventListShortcode']);
      add_action('pre_get_posts', [$this, 'paginationPerPage']);

    }


    /**
     * Register the custom post type of the plugin.
     * Clear the permalinks.
     *
     * @since    1.0.0
     */
    public function postTypeRegister()
    {
      $labels = array(
        'name' => _x(ucfirst($this->post_type_plural), 'Post Type General Name', $this->plugin_name),
        'singular_name' => _x(ucfirst($this->post_type), 'Post Type Singular Name', $this->plugin_name),
        'menu_name' => __(ucfirst($this->post_type_plural), $this->plugin_name),
        'name_admin_bar' => __(ucfirst($this->post_type), $this->plugin_name),
        'archives' => __('Item Archives', $this->plugin_name),
        'attributes' => __('Item Attributes', $this->plugin_name),
        'parent_item_colon' => __('Parent Item:', $this->plugin_name),
        'all_items' => __(sprintf('All %s', ucfirst($this->post_type_plural)), $this->plugin_name),
        'add_new_item' => __(sprintf('Add New %s', ucfirst($this->post_type)), $this->plugin_name),
        'add_new' => __(sprintf('Add %s', ucfirst($this->post_type)), $this->plugin_name),
        'new_item' => __('New Item', $this->plugin_name),
        'edit_item' => __(sprintf('Edit %s', ucfirst($this->post_type)), $this->plugin_name),
        'update_item' => __(sprintf('Update %s', ucfirst($this->post_type)), $this->plugin_name),
        'view_item' => __(sprintf('View %s', ucfirst($this->post_type)), $this->plugin_name),
        'view_items' => __(sprintf('View %s', ucfirst($this->post_type_plural)), $this->plugin_name),
        'search_items' => __(sprintf('Searcg %s', ucfirst($this->post_type)), $this->plugin_name),
        'not_found' => __('Not found', $this->plugin_name),
        'not_found_in_trash' => __('Not found in Trash', $this->plugin_name),
        'featured_image' => __('Featured Image', $this->plugin_name),
        'set_featured_image' => __('Set featured image', $this->plugin_name),
        'remove_featured_image' => __('Remove featured image', $this->plugin_name),
        'use_featured_image' => __('Use as featured image', $this->plugin_name),
        'insert_into_item' => __('Insert into item', $this->plugin_name),
        'uploaded_to_this_item' => __('Uploaded to this item', $this->plugin_name),
        'items_list' => __('Items list', $this->plugin_name),
        'items_list_navigation' => __('Items list navigation', $this->plugin_name),
        'filter_items_list' => __('Filter items list', $this->plugin_name),
      );
      $args = array(
        'label' => __(ucfirst($this->post_type), $this->plugin_name),
        'description' => __('Mange Custom post type', $this->plugin_name),
        'labels' => $labels,
        'supports' => array('title', 'editor', 'thumbnail', 'revisions'),
        'taxonomies' => array('category'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-calendar',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => $this->post_type_plural,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'show_in_rest' => true
      );
      register_post_type($this->post_type, $args);

      // Clear the permalinks after the post type has been registered.
      flush_rewrite_rules();
    }

    /**
     * Unregister the custom post type of the plugin.
     * Clear the permalinks.
     *
     * @since    1.0.0
     */
    public function postTypeUnRegister()
    {
      unregister_post_type($this->post_type);

      // remove post meta box
      remove_meta_box('event_information', $this->post_type, 'side');

      // Clear the permalinks to remove our post type's rules from the database.
      flush_rewrite_rules();
    }

    /**
     * Add post meta boxes for the event post type.
     *
     * @since    1.0.0
     */
    public function addPostMetaBoxes()
    {
      $screens = [$this->post_type];
      foreach ($screens as $screen) {
        add_meta_box(
          'event_information',
          esc_html__('Event', $this->plugin_name),
          [$this, 'eventPostMetaBoxFile'],
          $screen,
          'side'
        );
      }
    }

    /**
     * Load post meta box file for the event post type.
     *
     * @since    1.0.0
     * @param object $post post object
     */
    public function eventPostMetaBoxFile($post)
    {
      include plugin_dir_path(__FILE__) . 'partials/post-meta-event-information.php';
    }

    /**
     * Save meta box content for event post type
     * 
     * @since    1.0.0
     * @param int $post_id Post ID
     */
    public function saveMetaBox($post_id)
    {
      if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return;
      if ($parent_id = wp_is_post_revision($post_id)) {
        $post_id = $parent_id;
      }

      $fields = [
        '_event_start_date',
        '_event_end_date',
        '_event_location',
      ];

      foreach ($fields as $field) {
        if (array_key_exists($field, $_POST)) {
          update_post_meta($post_id, $field, sanitize_text_field($_POST[$field]));
        }
      }
    }

    /**
     * Create custom table heading for event list table.
     * 
     * @since    1.0.0
     * @param array $defaults Default table headings
     * @return array
     */
    public function eventTableHead($defaults)
    {
      $defaults['_event_start_date'] = __('Start Date', $this->plugin_name);
      $defaults['_event_end_date'] = __('End Status', $this->plugin_name);
      $defaults['_event_location'] = __('Location', $this->plugin_name);
      $defaults['author'] = __('Added By', $this->plugin_name);
      return $defaults;
    }

    /**
     * Create custom table heading for event list table.
     * 
     * @since    1.0.0
     * @param string $column_name Event list table column name
     * @param int $post_id Post Id
     */
    function eventTableContent($column_name, $post_id)
    {
      switch ($column_name) {
        case '_event_start_date':
          $start_date = get_post_meta($post_id, '_event_start_date', true);
          echo date(_x('F d, Y', 'Event start date format', $this->plugin_name), strtotime($start_date));
          break;

        case '_event_end_date':
          $end_date = get_post_meta($post_id, '_event_end_date', true);
          echo date(_x('F d, Y', 'Event end date format', $this->plugin_name), strtotime($end_date));
          break;

        case '_event_location':
          echo get_post_meta($post_id, '_event_location', true);
          break;
      }
    }

    /**
     * Create short code to display and filter events.
     * 
     * @since    1.0.0
     * @param array $atts Event list table column name
     * @param string $content Post Id
     * @return string
     */
    public function eventListShortcode($atts, $content = "")
    {
      $arg = shortcode_atts(
        array(
          'post_per_page' => $this->POST_PER_PAGE
        ),
        $atts
      );

      ob_start();
      include EVENT_MANAGEMENT_DIR . 'public/event/event-list.php';
      return ob_get_clean();
    }

    /**
     * event page pagination
     * 
     * @since    1.0.0
     * @return object WP_Query
     * @return object
     */
    function paginationPerPage($query)
    {
      $query->set('posts_per_page', $this->POST_PER_PAGE);
      return $query;
    }
  }
}