<?php

namespace Admin;

use Helper\EventManagement;

/**
 * Define the custom post type functionality
 *
 * Loads and defines the custom post type files for this plugin.
 *
 * @link       https://gitlab.com/wordpress_irn/swivel-assessment
 * @since      1.0.0
 *
 * @package    Event_Management
 * @subpackage Event_Management/admin
 */
if (!class_exists('CustomPostType')) {
  abstract class CustomPostType extends EventManagement
  {
    /**
     * The string of custom post type name.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $post_type    Post type name (singular)
     */
    protected $post_type;

    /**
     * The string of custom post type name (plural).
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $post_type_plural    Post type name (plural)
     */
    protected $post_type_plural;

    /**
     * Initialize the custom post type functionalities of the plugin.
     *
     * @since    1.0.0
     * @param string $post_type         Post type name (singular)
     * @param string $post_type_plural  Post type name (plural)
     */
    public function __construct($post_type, $post_type_plural)
    {
      $this->post_type = $post_type;
      $this->post_type_plural = $post_type_plural;

      add_action('init', [$this, 'postTypeRegister']);
    }

    /**
     * Abstract method of register the custom post type of the plugin.
     *
     * @since    1.0.0
     */
    abstract public function postTypeRegister();

    /**
     * Abstract method of unregister the custom post type of the plugin.
     *
     * @since    1.0.0
     */
    abstract public function postTypeUnRegister();
  }
}