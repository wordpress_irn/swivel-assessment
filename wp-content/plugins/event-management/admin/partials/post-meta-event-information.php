<div class="em_box">
  <style scoped>
    .em_field {
      margin-bottom: 15px;
      display: flex;
      flex-direction: column;
    }

    .em_field label {
      padding-bottom: 5px;
    }
  </style>
  <div class="meta-options em_field">
    <label for="_event_start_date">Start Date</label>
    <input id="_event_start_date" type="date" name="_event_start_date"
      value="<?php echo esc_attr(get_post_meta(get_the_ID(), '_event_start_date', true)); ?>">
  </div>
  <div class="meta-options em_field">
    <label for="_event_end_date">End Date</label>
    <input id="_event_end_date" type="date" name="_event_end_date"
      value="<?php echo esc_attr(get_post_meta(get_the_ID(), '_event_end_date', true)); ?>">
  </div>
  <div class="meta-options em_field">
    <label for="_event_location">Location</label>
    <input id="_event_location" type="text" name="_event_location" placeholder="eg: Colombo 03"
      value="<?php echo esc_attr(get_post_meta(get_the_ID(), '_event_location', true)); ?>">
  </div>
</div>