<?php

// pagination
if (get_query_var('paged')) {
  $paged = get_query_var('paged');
} else if (get_query_var('page')) {
  $paged = get_query_var('page');
} else {
  $paged = 1;
}

$_filter_args = array();
$searched_text = '';
$searched_location = '';
$searched_start_date = '';
$searched_end_date = '';

$default_args = array(
  'post_type' => 'event',
  'orderby' => 'date',
  'order' => 'DESC',
  'posts_per_page' => $arg['post_per_page'],
  'paged' => $paged
);

$_filter_args = $default_args;

$initial_form_data = array(
  "search" => "",
  "location" => "",
  "start_date" => "",
  "end_date" => "",
);

$form_url = get_post_type() === 'page' ? get_the_permalink() : get_site_url() . '/events';

if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] === 'GET') {

  // reset the form
  if (isset($_GET['reset'])) {
    wp_redirect($form_url);
  }

  $searched_data = array_diff($_GET, $initial_form_data);
  ksort($searched_data);

  if (!empty($_GET['search'])) {
    $searched_text = sanitize_text_field($_GET['search']);
    $search_text_filter_args = array(
      's' => $searched_text
    );
    $_filter_args = array_merge($default_args, $search_text_filter_args);
  }

  if (!empty($_GET['location'])) {
    $searched_location = sanitize_text_field($_GET['location']);
    $location_filter_args = array(
      'key' => '_event_location',
      'value' => $searched_location,
      'compare' => 'LIKE',
      'type' => 'CHAR'
    );
    $_filter_args['meta_query'][] = $location_filter_args;
  }

  if (!empty($_GET['start_date']) && empty($_GET['end_date'])) {
    $searched_start_date = sanitize_text_field($_GET['start_date']);
    $start_date_filter_args = array(
      'key' => '_event_start_date',
      'value' => $searched_start_date,
      'compare' => 'LIKE',
      'type' => 'DATE'
    );
    $_filter_args['meta_query'][] = $start_date_filter_args;
  }

  if (!empty($_GET['end_date']) && empty($_GET['start_date'])) {
    $searched_end_date = sanitize_text_field($_GET['end_date']);
    $end_date_filter_args = array(
      'key' => '_event_end_date',
      'value' => $searched_end_date,
      'compare' => 'LIKE',
      'type' => 'DATE'
    );
    $_filter_args['meta_query'][] = $end_date_filter_args;
  }

  if (!empty($_GET['start_date']) && !empty($_GET['end_date'])) {
    $searched_start_date = sanitize_text_field($_GET['start_date']);
    $searched_end_date = sanitize_text_field($_GET['end_date']);
    $start_date_filter_args = array(
      'key' => '_event_start_date',
      'value' => $searched_start_date,
      'compare' => '>=',
      'type' => 'DATE'
    );
    $end_date_filter_args = array(
      'key' => '_event_end_date',
      'value' => $searched_end_date,
      'compare' => '<=',
      'type' => 'DATE'
    );
    $_filter_args['meta_query'][] = $start_date_filter_args;
    $_filter_args['meta_query'][] = $end_date_filter_args;
  }
}

$event_list = new WP_Query($_filter_args);

?>

<section class="event-list">

  <div class="event-list__filter">
    <form role="form" action="<?php echo $form_url; ?>" method="get">
      <div class="form-group">
        <label for="event_search">Search</label>
        <input name="search" type="text" class="form-control" id="event_search" aria-describedby="eventSearch"
          placeholder="By title" value="<?php echo $searched_text; ?>">
      </div>
      <div class="form-group">
        <label for="event_location">Location</label>
        <input name="location" type="text" class="form-control" id="event_location" aria-describedby="eventLocation"
          placeholder="By location" value="<?php echo $searched_location; ?>">
      </div>
      <div class="form-group">
        <label for="start_date">Start Date</label>
        <input name="start_date" type="date" class="form-control" id="start_date" aria-describedby="eventStartDate"
          value="<?php echo $searched_start_date; ?>">
      </div>
      <div class="form-group">
        <label for="end_date">End Date</label>
        <input name="end_date" type="date" class="form-control" id="end_date" aria-describedby="eventEndDate"
          value="<?php echo $searched_end_date; ?>">
      </div>

      <div class="filter_btn">
        <button class="wp-block-button__link wp-element-button" type="submit">Filter</button>
        <button class="wp-block-button__link wp-element-button" name="reset" type="submit">Clear</button>
      </div>
    </form>
  </div>

  <div class="event-list__grid">
    <?php
    if ($event_list->have_posts()): ?>
      <div class="event-grid">

        <?php while ($event_list->have_posts()):

          $event_list->the_post();
          $event_ID = get_the_ID();
          $event_title = get_the_title();
          $event_link = get_post_permalink($event_ID);

          $image_url = get_the_post_thumbnail_url($event_ID) ? get_the_post_thumbnail_url($event_ID) : '';
          $image_alt = get_post_meta(get_post_thumbnail_id($event_ID), '_wp_attachment_image_alt', true);

          $event_start_date = get_post_meta($event_ID, '_event_start_date', true);
          $event_end_date = get_post_meta($event_ID, '_event_end_date', true);
          $event_location = get_post_meta($event_ID, '_event_location', true);

          include('event-card.php');

        endwhile; // end while ?>
      </div>

      <?php
    else: // else
      ?>
      <div class="event-empty-list">
        <p>
          <?php echo _e('Sorry, No events were found.', EVENT_MANAGEMENT_NAME); ?>
        </p>
      </div>
      <?
    endif; ?>

    <div class="pagination">
      <?php
      $big = 999999999; // need an unlikely integer
      
      echo paginate_links(
        array(
          'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
          'format' => '?paged=%#%',
          'current' => max(1, get_query_var('paged')),
          'total' => $event_list->max_num_pages
        )
      );
      ?>
    </div>
  </div>

</section>