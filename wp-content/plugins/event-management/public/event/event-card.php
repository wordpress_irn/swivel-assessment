<div class="event-card">

  <img class="img-fluid" src="<?php echo $image_url; ?>" srcset="<?php echo $srcset; ?>" alt="<?php echo $image_alt ?>"
    title="<?php echo $image_alt ?>" width="100%" />

  <a class="event-card__title" href="<?php echo $event_link; ?>">
    <?php echo $event_title; ?>
  </a>

  <?php if ($event_start_date): ?>
    <p class="event-card__date">
      <?php echo "Event Starts : " . $event_start_date; ?>
    </p>
  <?php endif; ?>

  <?php if ($event_end_date): ?>
    <p class="event-card__date">
      <?php echo "Event ends : " . $event_end_date; ?>
    </p>
  <?php endif; ?>

  <?php if ($event_location): ?>
    <p class="event-card--location">
      <?php echo $event_location; ?>
    </p>
  <?php endif; ?>

  <a class="event-card--link" href="<?php echo $event_link; ?>">Read More</a>

</div>