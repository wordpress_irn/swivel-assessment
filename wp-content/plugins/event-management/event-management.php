<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://gitlab.com/users/imran.fernando/contributed
 * @since             1.0.0
 * @package           Event_Management
 *
 * @wordpress-plugin
 * Plugin Name:       Event Management
 * Plugin URI:        https://gitlab.com/wordpress_irn/swivel-assessment
 * Description:       Simple plugin for event management 
 * Version:           1.0.0
 * Author:            Imran Fernando
 * Author URI:        https://gitlab.com/users/imran.fernando/contributed
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       event-management
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
defined('ABSPATH') or exit;

use Helper\EventManagement;

/*
 * Plugin constants
 */
define('EVENT_MANAGEMENT_VERSION', '1.0.0');
define('EVENT_MANAGEMENT_NAME', 'event-management');
define('EVENT_MANAGEMENT_DIR', plugin_dir_path(__FILE__));

// include the EventManagement class file
if (file_exists(plugin_dir_path(__FILE__) . 'includes/class-event-management.php')) {
  require_once plugin_dir_path(__FILE__) . 'includes/class-event-management.php';
}

// Begins execution of the plugin.
function run_event_management()
{
  $event_management = new EventManagement();

  register_activation_hook(__FILE__, [$event_management, 'activate']);
  register_deactivation_hook(__FILE__, [$event_management, 'deactivate']);

}

run_event_management();