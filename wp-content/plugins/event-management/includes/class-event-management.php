<?php

namespace Helper;

use Admin\Event;

/**
 * Define the event management functionality
 *
 * Loads and defines the event management files for this plugin.
 *
 * @link       https://gitlab.com/wordpress_irn/swivel-assessment
 * @since      1.0.0
 *
 * @package    Event_Management
 * @subpackage Event_Management/includes
 */

if (!class_exists('EventManagement')) {
  class EventManagement
  {
    protected $plugin_name;
    protected $plugin_version;
    protected $event;

    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the admin area and
     * the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function __construct()
    {
      $this->plugin_version = EVENT_MANAGEMENT_VERSION;
      $this->plugin_name = EVENT_MANAGEMENT_NAME;

      $this->loadDependencies();
      $this->setLocale();
    }

    /**
     * Load the required dependencies for this plugin.
     *
     * Create an instance of the event which will be used to register the hooks
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function loadDependencies()
    {
      // The class responsible for defining internationalization functionality
      require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-event-management-i18n.php';


      // The class responsible for creating custom post types
      require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-custom-post-type.php';
      require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-event.php';

      $this->event = new Event('event', 'events');
    }

    /**
     * Define the locale for this plugin for internationalization.
     *
     * Uses the Event_Management_i18n class in order to set the domain and to register the hook
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function setLocale()
    {
      $plugin_i18n = new EventManagementI18n();
      add_action('plugins_loaded', [$plugin_i18n, 'loadPluginTextDomain']);
    }

    /**
     * Fired during plugin activation
     *
     * @since      1.0.0
     * @access   public
     */
    public function activate()
    {
      $this->event->postTypeRegister();
    }

    /**
     * Fired during plugin deactivation
     *
     * @since      1.0.0
     * @access   public
     */
    public function deactivate()
    {
      $this->event->postTypeUnRegister();
    }
  }
}