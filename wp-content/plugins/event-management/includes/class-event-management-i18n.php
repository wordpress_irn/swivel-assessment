<?php

namespace Helper;

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://gitlab.com/wordpress_irn/swivel-assessment
 * @since      1.0.0
 *
 * @package    Event_Management
 * @subpackage Event_Management/includes
 */
if (!class_exists('EventManagementI18n')) {
  class EventManagementI18n
  {
    /**
     * Load the plugin text domain for translation.
     *
     * @since    1.0.0
     */
    public function loadPluginTextDomain()
    {
      load_plugin_textdomain(
        EVENT_MANAGEMENT_NAME,
        false,
        dirname(dirname(plugin_basename(__FILE__))) . '/languages/'
      );
    }
  }
}