# Swivel Assessment

Swivel Assessment requires [Docker](https://www.docker.com/).
The project uses docker images for mysql, adminer and wordpress.
Database sql file has inside the `mysql_db` folder and it will automatically imported.

**Clone with SSH**

WordPress theme and plugin that allows the user to manage events. The plugin allows the user to Add, Edit, and Delete events. The theme displays the events on the frontend using a shortcode. [Project Document](https://docs.google.com/document/d/1yHcG_bgCdM24C1fmxl5ArDgXd-oq8s7JBS6VrAQ9OeU/edit?usp=sharing)

**Clone with SSH**

```sh
git clone git@gitlab.com:wordpress_irn/swivel-assessment.git
```

**Clone with HTTPS**

```sh
git clone https://gitlab.com/wordpress_irn/swivel-assessment.git
```

**Go to the folder**

```sh
cd swivel-assessment
```

**Steps to initialize environment**

1. Crete .env file from env-sample and configure following parameters accordingly
   - `MYSQL_USER` Provide the database username. Don`t use root for the username. (Ex: username)
   - `MYSQL_PASSWORD` Provide the database password (Ex: userpassword)
   - `WORDPRESS_DEBUG` Provide the wordpress debug value (Ex: true)

**For development environments...**

```sh
docker compose up -d
cd wp-content/themes/swivelgroup
npm i
npm run dev
npm run watch (To watch the assets change)
```

**For production environments...**

```sh
docker compose up -d
cd wp-content/themes/swivelgroup
npm i
npm run build
```

**For removing docker containers...**

```sh
cd swivel-assessment
docker compose down
```

**Local server**
[http://localhost:8000](http://localhost:8000)

**Local server admin**
[http://localhost:8000/wp-admin](http://localhost:8000/wp-admin)

**Adminer**
[http://localhost:8080](http://localhost:8080)
